const config = require('./config');
const logger = require('./logger');
const Rocketeer = require('./lib/rocketeer');


exports = module.exports = new Rocketeer(config, logger);
exports.Rocketeer = Rocketeer;
