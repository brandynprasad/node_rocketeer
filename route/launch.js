const Router = require('express').Router;
const router = new Router();


function queryLaunchs(req, res) {
  req.logger.info('Querying for upcoming launchs');

  req.getLaunchData(req, (err, launchData) => {
    if (err) { return new Error(err); }
    if (!launchData) { return res.status(404).end(); }
    res.render('launch-list', {
      launchData, offset: parseInt(req.params.offset, 10)
    });
  });
}

router.get('/launch/:offset', queryLaunchs);


module.exports = router;
