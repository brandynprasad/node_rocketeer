const config = {
  server: {
    url            : 'http://localhost:8000',
    maxResultsLimit: 1000
  },
  logger: {
    console: {
      level                          : 'silly',
      timestamp                      : true,
      handleExceptions               : true,
      humanReadableUnhandledException: true,
      colorize                       : true
    }
  },
  launch: {
    url  : 'https://launchlibrary.net/1.3/launch',
    query: ''
  }
};


module.exports = config;
