node_rocketeer
==============

##### A tool for finding information about upcoming rocket launches!

- How to get up and running?
	- Have Node 6.2.0 or greater installed
	- Have NPM or Yarn package manager installed
	- `git clone` this repo
	- `cd` into the directory of the cloned repo
	- `npm install` or `yarn` to install dependencies
	- `npm start` or `yarn start` to fire up the server on localhost:8000
	- http://localhost:8000/launch/0 should now be accessible in your browser!
