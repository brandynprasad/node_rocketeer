const Server = require('./server');


class Rocketeer {

  constructor(config, logger) {
    this.config    = config;
    this.logger    = logger.child({ context: 'Rocketeer' });
    this.isRunning = false;

    this.server  = new Server(config, this.logger);
  }

  start(cb) {
    if (this.isRunning) {
      throw new Error('Cannot start Rocketeer because it is already running');
    }
    this.isRunning = true;

    this.logger.verbose('Starting Rocketeer');

    this.server.listen((err) => {
      if (err) { return cb(err); }

      this.logger.verbose('Rocketeer ready and awaiting requests');

      cb(null, { url: this.config.server.url });
    });
  }


  stop(cb) {
    if (!this.isRunning) {
      throw new Error('Cannot stop Rocketeer because it is already stopping');
    }
    this.isRunning = false;

    this.logger.verbose('Stopping Rocketeer');
    this.server.close((err) => {
      if (err) { return cb(err); }
      cb(null);
    });
  }
}


module.exports = Rocketeer;
