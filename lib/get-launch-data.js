const request = require('request');
const config  = require('../config');


let launches;

const assignAgencyData = (launchIndex, agencyId) => {
  request.get(`https://launchlibrary.net/1.3/agency/${agencyId}`, {
    json: true
  }, (err, clientRes) => {
    if (err) { return new Error(err); }

    if (!clientRes.body.agencies) {
      return new Error('Launch Library API responded without agency data');
    }

    Object.assign(launches[launchIndex].agency, {
      name       : clientRes.body.agencies[0].name,
      wikiURL    : clientRes.body.agencies[0].wikiURL,
      infoURLs   : clientRes.body.agencies[0].infoURLs,
      countryCode: clientRes.body.agencies[0].countryCode
    });
  });
};

const assignRocketData = (launchIndex, rocketId) => {
  request.get(`https://launchlibrary.net/1.3/rocket/${rocketId}`, {
    json: true
  }, (err, clientRes) => {
    if (err) { return new Error(err); }

    if (!clientRes.body.rockets) {
      return new Error('Launch Library API responded without rocket data');
    }

    Object.assign(launches[launchIndex].rocket, {
      name      : clientRes.body.rockets[0].name,
      wikiURL   : clientRes.body.rockets[0].wikiURL,
      infoURLs  : clientRes.body.rockets[0].infoURLs,
      imageURL  : clientRes.body.rockets[0].imageURL,
      imageSizes: clientRes.body.rockets[0].imageSizes
    });
  });
};

const assignLocationData = (launchIndex, locationId) => {
  request.get(`https://launchlibrary.net/1.3/location/${locationId}`, {
    json: true
  }, (err, clientRes) => {
    if (err) { return new Error(err); }

    if (!clientRes.body) {
      return new Error('Launch Library API responded without rocket data');
    }

    // We do not seem to get many results
    // based on the location ids we fetched
    if (clientRes.body.locations.length > 1) {
      Object.assign(launches[launchIndex].location, {
        name: clientRes.body.locations[0].name
      });
    } else {
      Object.assign(launches[launchIndex].location, {
        name: undefined
      });
    }
  });
};

const getLaunchData = (req, cb) => {
  req.logger.info('Fetching launch data from Launch Library API');

  const offset        = req.params.offset || 0;
  const currentDate   = new Date;
  const isoDateString = currentDate.toISOString();

  // Fetch upcoming launches based on current date
  let url = `${config.launch.url}?startdate=${isoDateString.slice(0, 10)}`;

  request.get(url, { json: true }, (err, clientRes) => {
    if (err) { return cb(err); }

    if (!clientRes.body) {
      return cb(new Error('Launch Library API responded without data'));
    }
    if (clientRes.body.total < 1) {
      return cb(new Error('Launch Library API responded with no upcoming launches'));
    }

    // Adding the fields param to specify which fields we want
    // No mode param used here!
    url = `${url}&offset=${offset}&fields=name,lsp,net,location,rocket,infoURLs,vidURLs`;

    request.get(url, { json: true }, (err, clientRes) => {
      if (err) { return cb(err); }

      if (!clientRes.body) {
        return cb(new Error('Launch Library API responded without launch data'));
      }

      launches = clientRes.body.launches.map(launch => ({
        id            : launch.id,
        net           : launch.net,
        infoURLs      : launch.infoURLs,
        vidURLs       : launch.vidURLs,
        agency: {
          id: parseInt(launch.lsp, 10)
        },
        location: {
          id: launch.locationid
        },
        rocket: {
          id: launch.rocketid
        }
      }));

      // We will use the ids we fetched to retrieve
      // the rest of the data required by the challenge
      for (const launch in launches) {

        // Find the agency name and links
        if (launches[launch].agency.id) {
          assignAgencyData(launch, launches[launch].agency.id);
        }

        // Find the rocket name and links
        if (launches[launch].rocket.id) {
          assignRocketData(launch, launches[launch].rocket.id);
        }

        // Find the location name
        if (launches[launch].location.id) {
          assignLocationData(launch, launches[launch].location.id);
        }
      }
    });
  });
  req.logger.info('Completed fetching launch data from Launch Library API');
  cb(null, launches);
};


module.exports = getLaunchData;
